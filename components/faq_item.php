<?php
    if (!isset($icon, $title, $description)){
        header("Location: ../pages/");
    }
?>

<li>
    <div class="collapsible-header polar-darken-3 white-text"><i class="material-icons"><?php echo $icon ?></i><?php echo $title ?></div>
    <div class="collapsible-body polar-darken-2"><span class="white-text"><?php echo $description ?></span></div>
</li>