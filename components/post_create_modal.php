<!-- JS for validation -->
<script src="../scripts/validation.js"></script>

<div id="newPost" class="modal polar-darken-3">
    <div class="modal-content white-text">
        <h4>New post</h4>
        <div class="row">
            <form class="col s12 m12" id="newPostForm" method="post" onsubmit="return checkNewPost(this, 'text')" action="../helpers/edit_post.php?action=create&type=text">
                <div class="row">
                    <div class="input-field col s12">
                        <input name="title" id="title" type="text" class="grey-text text-lighten-3" oninput="checkTitle(this)">
                        <label for="title">Title</label>
                        <span id="titleMessage" class="helper-text grey-text" data-error="An error occurred!">Title cannot be empty! Post won't be public if the title is empty.</span>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <textarea name="description" id="description" class="grey-text text-lighten-3 materialize-textarea" oninput="checkDescription(this)"></textarea>
                        <label for="description">Description</label>
                        <span id="descriptionMessage" class="helper-text" data-error="An error occurred"></span>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer polar-darken-4">
        <a class="modal-close waves-effect waves-green btn-flat aurora-red white-text" onclick="document.getElementById('newPostForm').reset()">Cancel</a>
        <a id="modalDeleteButton" onclick="document.getElementById('newPostForm').submit()" class="waves-effect waves-green btn-flat blue white-text">Post</a>
    </div>
</div>