<?php
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
    if (!isset($_SESSION["loggedIn"]) && !$db->isAdmin($_SESSION["username"])) {
        exit();
    }
?>

<li class="collection-item avatar gray">
    <img src="<?php
        if(isset($row["email"])){
            $email = $row["email"];
        }
        include("gravatar.php");
        echo $gravatar;
    ?>" alt="Profile Picture" class="circle">
    <span class="title"><?php echo $row["username"] ?></span>
    <a class="right modal-trigger tooltipped" data-position="right" data-tooltip="Delete user" onclick="deleteModalSetup(<?php echo $row['user_id'].", ".$row['active'] ?>)" href="#deleteUser"><i class="material-icons red-text">delete</i></a>
    <?php
        if ($row["active"] == 1){
            echo '<a class="right tooltipped" data-position="bottom" data-tooltip="Deactivate user" href="../helpers/edit_user.php?id='.$row["user_id"].'&action=block"><i class="material-icons orange-text">block</i></a>';
        }
        else {
            echo '<a class="right tooltipped" data-position="bottom" data-tooltip="Activate user" href="../helpers/edit_user.php?id='.$row["user_id"].'&action=unblock"><i class="material-icons green-text">check</i></a>';
        }
    ?>
    <a class="right tooltipped" data-position="left" data-tooltip="Edit user" href="../helpers/edit_user.php?id=<?php echo $row["user_id"] ?>&action=edit"><i class="material-icons">edit</i></a>
    <p>
        <?php
            $name = $email = "";
            if(isset($row["firstname"]) && $row["firstname"] != ""){
                $name = $row["firstname"]." ";
            }
            if (isset($row["last_name"]) && $row["last_name"] != ""){
                $name .=$row["last_name"]." ";
            }
            if(isset($row["email"]) && $row["email"] != ""){
               $email = $row["email"];
            }
            if ($name != "" && $email != ""){
                $name .= " - ";
            }
            echo $name.$email;
        ?>
        <br>
        <?php
            echo "Posts: ".$row["post_count"];
            if ($row["admin"] == 1){
                echo '<a class="right tooltipped" data-position="left" data-tooltip="Demote to user" href="../helpers/edit_user.php?id='.$row["user_id"].'&action=demote"><i class="material-icons green-text">security</i></a>';
            }
            else{
                echo '<a class="right tooltipped" data-position="left" data-tooltip="Promote to admin" href="../helpers/edit_user.php?id='.$row["user_id"].'&action=promote"><i class="material-icons blue-grey-text">security</i></a>';
            }
        ?>
    </p>
</li>