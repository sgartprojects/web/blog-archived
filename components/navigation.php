<?php
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
?>
<header>
    <nav class="polar-darken-4 hide-on-large-only">
        <a href="" data-target="slide-out" class="sidenav-trigger white-text"><i class="material-icons">menu</i></a>
    </nav>
</header>
<ul id="slide-out" class="sidenav sidenav-fixed polar-darken-4">
    <li>
        <div class="user-view">
            <div class="background">
                <img src="../assets/user_background.jpg" alt="Profile Background">
            </div>
            <img class="circle center-block" alt="Profile Picture" src="<?php
                if (isset($_SESSION['pfp']) && $_SESSION['pfp'] != null){
                    echo $_SESSION['pfp'];
                }
                else{
                    echo 'https://gravatar.com/avatar/?d=identicon';
                }
            ?>">
            <span class="white-text name center-align">
                        <?php
                        if (isset($_SESSION["displayName"])){
                            echo $_SESSION["displayName"];
                        }
                        else {
                            echo "Anonymous";
                        }
                        ?>
                    </span>
            <span class="email"></span>
        </div>
    </li>
    <li><a href="../pages/" class="white-text"><i class="material-icons white-text">home</i>Home</a></li>
    <li><div class="divider"></div></li>
    <?php
    if (isset($_SESSION["loggedIn"])):
    ?>
        <li><a href="../pages/my_posts.php" class="white-text"><i class="material-icons white-text">chat_bubble</i>My posts</a></li>
        <li><a href="../pages/settings.php" class="white-text"><i class="material-icons white-text">settings</i>Settings</a></li>
        <?php
        if ($db->isAdmin($_SESSION["username"])):
        ?>
            <li><div class="divider"></div></li>
            <li><a href="../pages/users.php" class="white-text"><i class="material-icons white-text">group</i>Users</a></li>
        <?php
        endif;
        ?>
        <li><div class="divider"></div></li>
        <li><a href="../pages/faq.php" class="white-text"><i class="material-icons white-text">question_answer</i>FaQ</a></li>
        <li><div class="divider"></div></li>
        <li><a href="../helpers/logout.php" class="white-text"><i class="material-icons white-text">person</i>Log out</a></li>
    <?php
    else:
    ?>
        <li><a href="../pages/faq.php" class="white-text"><i class="material-icons white-text">question_answer</i>FaQ</a></li>
        <li><div class="divider"></div></li>
        <li><a href="../pages/login.php" class="white-text"><i class="material-icons white-text">person</i>Log in</a></li>
    <?php
    endif;
    ?>
</ul>