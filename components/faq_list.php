<ul class="collapsible popout">
    <?php
        if (!isset($db)) {
            include("../components/database.php");
            $db = new Database();
        }
        if (isset($term)){
            $rows = $db->getFaqItemsBySearch($term);
        }
        else {
            $rows = $db->getFaqItems();
        }
        foreach($rows as $row){
            if ($row["icon"] == ""){
                $icon = "question_answer";
            }
            else {
                $icon = $row["icon"];
            }
            $title = $row["title"];
            $description = $row["description"];
            include("faq_item.php");
        }
    ?>
</ul>