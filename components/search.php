<?php
    $value = "";
    if (isset($term)){
        $value = $term;
    }
?>
<script src="../scripts/validation.js"></script>
<form method="post" onsubmit="return checkSearch(this)" class="col s12 m7" action="../pages/faq.php">
    <div class="input-field col s12">
        <i class="material-icons prefix white-text">search</i>
        <input placeholder="Search for something" id="term" name="term" type="text" class="white-text" onkeypress="checkInput(this)" value="<?php echo $value ?>">
        <span class="helper-text" data-error="Search term can only contain letters and spaces!"></span>
    </div>
    <button type="submit" hidden="hidden"></button>
</form>