<?php
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }

    if (!isset($_SESSION["loggedIn"]) && !$db->isAdmin($_SESSION["username"])){
        Header("Location: ./");
        exit();
    }

    if (isset($_GET["action"]) && $_GET["action"] == "edit" && isset($_GET["id"])){
        $user = $db->getUserById($_GET["id"]);
        $username = $user["username"];
        $firstname = $user["firstname"];
        $lastName = $user["lastname"];
        $email = $user["email"];
        $id = $_GET["id"];

        $validation = "edit(this)";
        $submitAction = "update";

        $_GET["action"] = "update";

        $tabTitle = "Edit user - Blog";
    }
    else if(isset($_GET["action"]) && $_GET["action"] == "registration" && isset($_GET["id"])){
        $user = $db->getUserByName($_SESSION["username"]);
        $username = $user["username"];
        $firstname = $user["firstname"];
        $lastName = $user["lastname"];
        $email = $user["email"];
        $id = $_GET["id"];

        $validation = "registration(this)";
        $submitAction = "create";

        $tabTitle = "New user - Blog";
    }
    else {
        die("An error occurred!");
    }
?>
<html lang="en">
    <head>
        <!-- Title -->
        <title>
            <?php
                echo $tabTitle;
            ?>
        </title>

        <!-- Icon -->
        <link rel="icon" type="img/png" href="../assets/logo.png">

        <!-- Meta -->
        <meta name="author" content="Samuel Gartmann">
        <meta name="description" content="This page was made as a school project in 2020">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="../css/north.colorscheme.css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- JS to check data -->
        <script src="../scripts/validation.js" type="text/javascript"></script>
    </head>
    <body class="polar-darken-4">
        <?php
            include("../components/navigation.php");
        ?>
        <main>
            <div class="row">
                <div class="col s0 m3 hide-on-med-and-down"></div>
                <div class="col s12 m12 l6">
                    <div class="row hide-on-small-and-down"></div>
                    <div class="card polar-darken-3">
                        <div class="card-content">
                            <div class="row">
                                <form class="col s12 m12" method="post" onsubmit="return <?php echo $validation ?>" action="../helpers/edit_user.php?id=<?php echo $id ?>&action=<?php echo $submitAction ?>">
                                    <div class="row">
                                        <div class="input-field col s12 m6">
                                            <input name="firstName" id="firstName" type="text" class="grey-text text-lighten-3" oninput="checkFirstName(this)"
                                                <?php
                                                    if (isset($firstname)){
                                                        echo "value='$firstname'";
                                                    }
                                                ?>
                                            >
                                            <label for="firstName">Firstname (Optional)</label>
                                            <span id="firstNameMessage" class="helper-text" data-error="An error occurred!"></span>
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <input name="lastName" id="lastName" type="text" class="grey-text text-lighten-3" oninput="checkLastName(this)"
                                               <?php
                                                    if (isset($lastName)){
                                                        echo "value='$lastName'";
                                                    }
                                               ?>
                                            >
                                            <label for="lastName">Surname (Optional)</label>
                                            <span id="lastNameMessage" class="helper-text" data-error="An error occurred!"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php
                                            if (isset($_GET["action"]) && $_GET["action"] == "registration"):
                                        ?>
                                            <div class="input-field col s12 m6">
                                                <input name="username" id="username" type="text" class="grey-text text-lighten-3" disabled="disabled">
                                                <label for="username">Username</label>
                                                <span id="usernameMessage" class="helper-text" data-error="Username cannot be empty!"></span>
                                            </div>
                                        <?php
                                            endif;
                                        ?>
                                        <div class="input-field col s12 m6">
                                            <input name="email" id="email" type="email" class="grey-text text-lighten-3" oninput="checkEmail(this)"
                                                <?php
                                                    if (isset($email)){
                                                        echo "value=$email";
                                                    }
                                                ?>
                                            >
                                            <label for="email">Email (Optional)</label>
                                            <span id="emailMessage" class="helper-text" data-error="Please enter a valid email!"></span>
                                        </div>
                                    </div>
                                        <?php
                                            if (isset($_GET["action"]) && $_GET["action"] == "registration"):
                                        ?>
                                            <div class="row">
                                                <div class="input-field col s12 m6">
                                                    <input name="password" id="password" type="password" class="grey-text text-lighten-3" oninput="checkPassword(this)">
                                                    <label for="password">Password</label>
                                                    <span id="passwordMessage" class="helper-text" data-error="Password cannot be empty!"></span>
                                                </div>
                                                <div class="input-field col s12 m6">
                                                    <input name="passwordRedo" id="passwordRedo" type="password" class="grey-text text-lighten-3" oninput="checkPasswordRedo(this)">
                                                    <label for="passwordRedo">Reenter password</label>
                                                    <span id="passwordRedoMessage" class="helper-text" data-error="Passwords don\'t match!"></span>
                                                </div>
                                            </div>
                                        <?php
                                            endif;
                                        ?>
                                    <div class="row">
                                        <button id="btn-cancel" class="waves-effect waves-light btn polar-darken-1 left" type="button" onclick="window.location.replace('users.php')"><i class="material-icons left">cancel</i>
                                            Cancel
                                        </button>
                                        <button id="btn-registration" class="waves-effect waves-light btn polar-darken-1 right" type="submit"><i class="material-icons left">
                                                <?php
                                                    if (isset($_GET["action"]) && $_GET["action"] == "update"){
                                                        echo "save</i>Save changes";
                                                    }
                                                    else if (isset($_GET["action"]) && $_GET["action"] == "registration"){
                                                        echo "person_add</i>Register new account";
                                                    }
                                                ?>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="../scripts/materialize.min.js"></script>
        <?php
            if (isset($_GET["success"]) && $_GET["success"] == 'false'):
        ?>
            <script>
                M.toast({html: 'Data validation failed! Please try again...', classes: 'aurora-red'})
            </script>
        <?php
            endif;
        ?>
    </body>
</html>