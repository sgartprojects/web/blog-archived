<?php
    session_start();
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
    if (isset($_GET["action"])){
        $posts = $db->getPostById($_GET["id"]);
        foreach ($posts as $post){
            $post_id = $post["post_id"];
            $title = $post["title"];
            $description = $post["description"];
            $type = $post["image_path"] == "" ? "text" : "image";
        }
    }
    else{
        Header("Location: ../pages/");
        exit();
    }
?>
<html lang="en">
    <head>
        <!-- Title -->
        <title>Edit post - Blog</title>

        <!-- Icon -->
        <link rel="icon" type="img/png" href="../assets/logo.png">

        <!-- Meta -->
        <meta name="author" content="Samuel Gartmann">
        <meta name="description" content="This page was made as a school project in 2020">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="../css/north.colorscheme.css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- JS to check data -->
        <script src="../scripts/validation.js" type="text/javascript"></script>
    </head>
    <body class="polar-darken-4">
        <?php
            include("../components/navigation.php");
        ?>
        <main>
            <div class="row">
                <div class="col s0 m3 hide-on-med-and-down"></div>
                <div class="col s12 m12 l6">
                    <div class="row hide-on-small-and-down"></div>
                    <div class="card polar-darken-3">
                        <div class="card-content">
                            <div class="row">
                                <form class="col s12 m12" method="post" onsubmit="return checkPostEdit(this)" action="../helpers/edit_post.php?action=update&id=<?php echo $_GET["id"]?>&title=<?php echo $title ?>&desc=<?php echo $description ?>">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input name="title" id="title" type="text" class="grey-text text-lighten-3" oninput="checkTitle(this)"
                                                <?php
                                                    echo "value=\"$title\"";
                                                ?>
                                            >
                                            <label for="title">Title <?php echo $type == "image" ? "(optional)" : "" ?></label>
                                            <span id="titleMessage" class="helper-text grey-text" data-error="An error occurred!"><?php echo $type == "image" ? "" : "Title cannot be empty! Post won't be public if the title is empty." ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <textarea name="description" id="description" class="grey-text text-lighten-3 materialize-textarea" oninput="checkDescription(this)"
                                            ><?php echo $description; ?></textarea>
                                            <label for="description">Description</label>
                                            <span id="descriptionMessage" class="helper-text" data-error="An error occurred"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button id="btn-cancel" class="waves-effect waves-light btn polar-darken-1 left" type="button" onclick="window.location.replace('my_posts.php')"><i class="material-icons left">cancel</i>
                                            Cancel
                                        </button>
                                        <button id="btn-save" class="waves-effect waves-light btn polar-darken-1 right" type="submit"><i class="material-icons left">save</i>
                                            Save changes
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="../scripts/materialize.min.js"></script>
        <?php
            if (isset($_GET["success"]) && $_GET["success"] == 'false'):
        ?>
                <script>
                    M.toast({html: 'Data validation failed! Please try again...', classes: 'aurora-red'})
                </script>
        <?php
            endif;
        ?>
    </body>
</html>