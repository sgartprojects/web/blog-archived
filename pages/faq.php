<?php
    session_start();
    if (isset($_GET["search"]) && $_GET["search"] != ""){
        $term = $_GET["search"];
    }
    else if (isset($_POST["term"]) && $_POST["term"] != ""){
        $term = $_POST["term"];
    }
?>
<html lang="en">
    <head>
        <!-- Title -->
        <title>Faq - Blog</title>

        <!-- Icon -->
        <link rel="icon" type="img/png" href="../assets/logo.png"

        <!-- Meta -->
        <meta name="author" content="Samuel Gartmann">
        <meta name="description" content="This page was made as a school project in 2020">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="../css/north.colorscheme.css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- JS for sidenav -->
        <script src="../scripts/materialize.min.js" type="text/javascript"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var themeSelector = document.querySelectorAll('select');
                M.FormSelect.init(themeSelector);
                const collapsible = document.querySelectorAll('.collapsible');
                M.Collapsible.init(collapsible);
            });
        </script>
    </head>
    <body class="polar-darken-4">
        <?php
            include("../components/navigation.php");
        ?>
        <main>
            <div class="row hide-on-med-and-down"></div>
            <div class="row">
                <div class="col s0 m2 hide-on-med-and-down"></div>
                <?php
                    include("../components/search.php");
                ?>
            </div>
            <div class="row">
                <div class="col s0 m2 hide-on-med-and-down"></div>
                <div class="col s12 m7">
                    <?php
                        include("../components/faq_list.php");
                    ?>
                </div>
            </div>
        </main>
    </body>
</html>
