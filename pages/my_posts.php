<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Title -->
        <title>My posts - Blog</title>

        <!-- Icon -->
        <link rel="icon" type="img/png" href="../assets/logo.png"

        <!-- Meta -->
        <meta name="author" content="Samuel Gartmann">
        <meta name="description" content="This page was made as a school project in 2020">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="../css/north.colorscheme.css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- JS for sidenav -->
        <script src="../scripts/materialize.min.js" type="text/javascript"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                let sideNav = document.querySelectorAll('.sidenav');
                M.Sidenav.init(sideNav);
                let tooltips = document.querySelectorAll('.tooltipped');
                M.Tooltip.init(tooltips);
                let picturePopup = document.querySelectorAll('.materialboxed');
                M.Materialbox.init(picturePopup);
                let modalElem = document.querySelectorAll('.modal');
                M.Modal.init(modalElem);
            });

            function deleteModalSetup(id, archived){
                document.getElementById("modalArchiveButton").href="../helpers/edit_post.php?action=archive&id="+id;
                document.getElementById("modalDeleteButton").href="../helpers/edit_post.php?action=delete&id="+id;
                if (archived){
                    document.getElementById("modalArchiveButton").classList.add("hide");
                    document.getElementById("modalDescription").classList.add("hide");
                }
                else{
                    document.getElementById("modalArchiveButton").classList.remove("hide");
                    document.getElementById("modalDescription").classList.remove("hide");
                }
            }
        </script>
    </head>
    <body class="polar-darken-4">
        <?php
            include("../components/navigation.php");
        ?>
        <main>
            <div class="row">
                <?php
                    $username = $_SESSION["username"];
                    $edit = true;
                    include("../helpers/print_posts.php");
                ?>
            </div>
            <div class="fixed-action-btn show-on-small">
                <a class="btn-floating btn-large green modal-trigger" href="#newPost">
                    <i class="material-icons">add_circle_outline</i>
                </a>
            </div>
            <div id="deletePost" class="modal polar-darken-3 white-text">
                <div class="modal-content">
                    <h4>Are you sure you want to delete this post? It will be lost forever!</h4>
                    <p id="modalDescription">Alternatively you can archive a post to prevent it form showing in the public feed.</p>
                </div>
                <div class="modal-footer polar-darken-3">
                    <a class="modal-close waves-effect waves-green btn-flat blue white-text"><i class="material-icons left">cancel</i>No, I want to keep it!</a>
                    <a id="modalArchiveButton" href="" class="modal-close waves-effect waves-green btn-flat aurora-orange white-text hide"><i class="material-icons left">archive</i>No, archive the post instead!</a>
                    <a id="modalDeleteButton" href="" class="modal-close waves-effect waves-green btn-flat aurora-red white-text"><i class="material-icons left">delete_forever</i>Yes, delete it!</a>
                </div>
            </div>
            <?php
                require("../components/post_create_modal.php");
            ?>
        </main>
        <script src="../scripts/materialize.min.js"></script>
        <?php
            if (isset($_GET["edit"]) && $_GET["edit"] == 'failed'):
        ?>
                <script>
                    M.toast({html: 'Could not save changes!', classes: 'aurora-red'})
                </script>";
        <?php
            endif;
        ?>
        <?php
            if (isset($_GET["edit"]) && $_GET["edit"] == 'success'):
        ?>
            <script>
                M.toast({html: 'Changes have been saved!', classes: 'aurora-green'})
            </script>";
        <?php
            endif;
        ?>
    </body>
</html>