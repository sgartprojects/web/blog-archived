<?php
    session_start();
?>
<html lang="en">
    <head>
        <!-- Title -->
        <title>Home - Blog</title>

        <!-- Icon -->
        <link rel="icon" type="img/png" href="../assets/logo.png"

        <!-- Meta -->
        <meta name="author" content="Samuel Gartmann">
        <meta name="description" content="This page was made as a school project in 2020">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="../css/north.colorscheme.css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- JS for sidenav -->
        <script src="../scripts/materialize.min.js" type="text/javascript"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                let sideNav = document.querySelectorAll('.sidenav');
                M.Sidenav.init(sideNav);
                let picturePopup = document.querySelectorAll('.materialboxed');
                M.Materialbox.init(picturePopup);
            });
        </script>
    </head>
    <body class="polar-darken-4">
        <?php
            include("../components/navigation.php");
        ?>
        <main>
            <div class="row">
                <?php
                    $amount = 24;
                    include("../helpers/print_posts.php");
                ?>
            </div>
        </main>
        <script src="../scripts/materialize.min.js"></script>
        <?php
            if (isset($_GET["loginSuccess"]) && $_GET["loginSuccess"] == 'true'):
        ?>
                <script>
                    M.toast({html: 'Login successful!', classes: 'aurora-green'})
                </script>";
        <?php
            endif;
        ?>
    </body>
</html>