<?php
    session_start();
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
    if (!isset($_SESSION["loggedIn"]) && !$db->isAdmin($_SESSION["username"])){
        Header("Location: ./");
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Title -->
        <title>Users - Blog</title>

        <!-- Icon -->
        <link rel="icon" type="img/png" href="../assets/logo.png"

        <!-- Meta -->
        <meta name="author" content="Samuel Gartmann">
        <meta name="description" content="This page was made as a school project in 2020">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="../css/north.colorscheme.css">
        <link rel="stylesheet" type="text/css" href="../css/custom.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- JS for sidenav -->
        <script src="../scripts/materialize.min.js" type="text/javascript"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                let sideNav = document.querySelectorAll('.sidenav');
                M.Sidenav.init(sideNav);
                var modals = document.querySelectorAll('.modal');
                M.Modal.init(modals);
                var tooltip = document.querySelectorAll('.tooltipped');
                M.Tooltip.init(tooltip);
            });

            function deleteModalSetup(id, active){
                document.getElementById("modalArchiveButton").href="../helpers/edit_user.php?action=block&id="+id;
                document.getElementById("modalDeleteButton").href="../helpers/edit_user.php?action=delete&id="+id;
                if (active){
                    document.getElementById("modalArchiveButton").classList.remove("hide");
                    document.getElementById("modalDescription").classList.remove("hide");
                }
                else{
                    document.getElementById("modalArchiveButton").classList.add("hide");
                    document.getElementById("modalDescription").classList.add("hide");
                }
            }
        </script>
    </head>
    <body class="polar-darken-4">
        <?php
            require("../components/navigation.php");
        ?>
        <main>
            <?php
                include("../helpers/print_users.php");
            ?>

            <div class="fixed-action-btn small">
                <a class="btn-floating btn-large green tooltipped disabled" data-position="left" data-tooltip="Create new user" href="">
                    <i class="large material-icons">add_circle_outline</i>
                </a>
            </div>
            <div id="deleteUser" class="modal polar-darken-3">
                <div class="modal-content white-text">
                    <h4>Are you sure you want to delete this account? <br> It will be lost forever!</h4>
                    <p id="modalDescription">Alternatively you can prevent a user from logging in by blocking the account instead of deleting it.</p>
                </div>
                <div class="modal-footer polar-darken-3">
                    <a class="modal-close waves-effect waves-green btn-flat blue white-text"><i class="material-icons left">cancel</i>No, I want to keep it!</a>
                    <a id="modalArchiveButton" href="" class="modal-close waves-effect waves-green btn-flat aurora-orange white-text hide"><i class="material-icons left">block</i>No, block the account instead!</a>
                    <a id="modalDeleteButton" href="" class="modal-close waves-effect waves-green btn-flat aurora-red white-text"><i class="material-icons left">delete_forever</i>Yes, delete it!</a>
                </div>
            </div>
        </main>
        <script src="../scripts/materialize.min.js"></script>
        <?php
            if (isset($_GET["action"]) && $_GET["action"] == 'saved'):
        ?>
                <script>
                    M.toast({html: 'Changes have been saved!', classes: 'aurora-green'})
                </script>
        <?php
            endif;
        ?>
    </body>
</html>
