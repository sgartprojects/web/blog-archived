<?php
    session_start();
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
    include('../components/gravatar.php');
?>
<html lang="en">
<head>
    <!-- Title -->
    <title>Settings - Blog</title>

    <!-- Icon -->
    <link rel="icon" type="img/png" href="../assets/logo.png"

    <!-- Meta -->
    <meta name="author" content="Samuel Gartmann">
    <meta name="description" content="This page was made as a school project in 2020">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="../css/north.colorscheme.css">
    <link rel="stylesheet" type="text/css" href="../css/custom.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- JS for validation -->
    <script src="../scripts/validation.js" type="text/javascript"></script>

    <!-- JS for sidenav -->
    <script src="../scripts/materialize.min.js" type="text/javascript"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let sideNav = document.querySelectorAll('.sidenav');
            M.Sidenav.init(sideNav);
            let tabs = document.querySelectorAll('.tabs');
            let tabInstance = M.Tabs.init(tabs);
            <?php
                if (isset($_GET["tab"])):
            ?>
                    tabInstance.select('appearance');
            <?php
                endif;
            ?>
        });
    </script>
</head>
<body class="polar-darken-4">
<?php
    include("../components/navigation.php");
?>
<main>
    <div class="row">
        <div class="col s0 m2 hide-on-small-and-down"></div>
        <div class="col s12 m7">
            <div class="card horizontal polar-darken-3">
                <div class="card-image">
                    <img src="<?php
                        if (isset($_SESSION['pfp']) && $_SESSION['pfp'] != null){
                            echo $_SESSION['pfp'];
                        }
                        else{
                            echo 'https://gravatar.com/avatar/?d=identicon';
                        }
                    ?>" alt="Avatar" class="responsive-img">
                </div>
                <div class="card-stacked">
                    <div class="card-content white-text">
                        <p>Visit <a href="../pages/faq.php">the faq</a> on how to change your profile picture</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s0 m2 hide-on-small-and-down"></div>
        <div class="col s12 m7">
            <div class="card horizontal polar-darken-3">
                <div class="card-stacked">
                    <div class="row">
                        <div class="col s12">
                                <ul class="tabs polar-darken-3">
                                    <li class="tab col s4"><a class="active white-text" href="#myAccount">My account</a></li>
                                    <li class="tab col s4"><a class="white-text" href="#changePassword">Change password</a></li>
                                    <li class="tab col s4"><a class="white-text" href="#appearance">Appearance</a></li>
                                </ul>
                        </div>
                    </div>
                    <div class="row"></div>
                    <?php
                        $user = $db->getUserByName($_SESSION["username"]);
                        $id = $user["userId"];
                        $firstname = $user["firstname"];
                        $lastName = $user["lastname"];
                        $email = $user["email"];
                        $diplayName = $user["displayName"];
                    ?>
                    <div class="row">
                        <div id="myAccount" class="col s12">
                            <form class="col s12 m12" method="post" onsubmit="return edit(this)" action="../helpers/edit_user.php?id=<?php echo $id ?>&action=update&redirect=settings">
                                <div class="row">
                                    <div class="col s3"></div>
                                    <div class="input-field col s6">
                                        <input name="firstName" id="firstName" type="text" class="grey-text text-lighten-3" oninput="checkFirstName(this)"
                                            <?php
                                                if (isset($firstname)){
                                                    echo "value='$firstname'";
                                                }
                                            ?>
                                        >
                                        <label for="firstName">Firstname (Optional)</label>
                                        <span id="firstNameMessage" class="helper-text" data-error="An error occurred!"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s3"></div>
                                    <div class="input-field col s6">
                                        <input name="lastName" id="lastName" type="text" class="grey-text text-lighten-3" oninput="checkLastName(this)"
                                            <?php
                                                if (isset($lastName)){
                                                    echo "value='$lastName'";
                                                }
                                            ?>
                                        >
                                        <label for="lastName">Surname (Optional)</label>
                                        <span id="lastNameMessage" class="helper-text" data-error="An error occurred!"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s3"></div>
                                    <div class="input-field col s6">
                                        <input name="email" id="email" type="email" class="grey-text text-lighten-3" oninput="checkEmail(this)"
                                            <?php
                                                if (isset($email)){
                                                    echo "value=$email";
                                                }
                                            ?>
                                        >
                                        <label for="email">Email (Optional)</label>
                                        <span id="emailMessage" class="helper-text" data-error="Please enter a valid email!"></span>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col s3"></div>
                                    <div class="col s6">
                                        <button id="btn-save" class="waves-effect waves-light btn polar-darken-1 right" type="submit"><i class="material-icons left">save</i>Save changes
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="changePassword" class="col s12">
                            <div class="row">
                                <form class="col s12 m12" method="post" onsubmit="return changePassword(this)" action="../helpers/edit_user.php?id=<?php echo $id ?>&action=changePassword">
                                    <div class="row">
                                        <div class="col s3"></div>
                                        <div class="input-field col s6">
                                            <input name="oldPassword" id="oldPassword" type="password" class="grey-text text-lighten-3" oninput="checkPassword(this)">
                                            <label for="oldPassword">Old password</label>
                                            <span id="oldPasswordMessage" class="helper-text" data-error="An error occurred!"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s3"></div>
                                        <div class="input-field col s6">
                                            <input name="newPassword" id="newPassword" type="password" class="grey-text text-lighten-3" oninput="checkPassword(this)">
                                            <label for="newPassword">New Password</label>
                                            <span id="newPasswordMessage" class="helper-text" data-error="An error occurred!"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s3"></div>
                                        <div class="input-field col s6">
                                            <input name="newPasswordRedo" id="newPasswordRedo" type="password" class="grey-text text-lighten-3" oninput="checkNewPasswordRedo(this)">
                                            <label for="newPasswordRedo">Repeat new password</label>
                                            <span id="newPasswordRedoMessage" class="helper-text" data-error="Please enter a valid email!"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s3"></div>
                                        <div class="col s6">
                                            <button id="btn-save" class="waves-effect waves-light btn polar-darken-1 right disabled" type="submit"><i class="material-icons left">save</i>Change password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="appearance" class="col s12">
                            <div class="row">
                                <form class="col s12 m12" method="post" action="../helpers/edit_user.php?id=<?php echo $id ?>&action=setDisplayName">
                                    <div class="row">
                                        <div class="col s3"></div>
                                        <div class="input-field col s6 white-text">
                                            <h5>Change display name</h5>
                                            <span class="grey-text">Change the name shown in the navigation. Other users will never see you real name, regardless on what you selected.</span>
                                            <p>
                                                <label>
                                                    <input name="displayName" value="realName" type="radio" <?php echo $diplayName == "realName" ? "checked" : ""?>/>
                                                    <span class="white-text">First & Lastname</span>
                                                </label>
                                            </p>
                                            <p>
                                                <label>
                                                    <input name="displayName" value="username" type="radio" <?php echo $diplayName == "username" ? "checked" : ""?>/>
                                                    <span class="white-text">Username</span>
                                                </label>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s3"></div>
                                        <div class="col s6">
                                            <button id="btn-save" class="waves-effect waves-light btn polar-darken-1 right" type="submit"><i class="material-icons left">save</i>Save changes
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        if (isset($_GET["edit"])):
            if ($_GET["edit"] == "success"):
    ?>
                <script>
                    M.toast({html: 'Changes successfully saved!', classes: 'aurora-green'})
                </script>
    <?php
            endif;
            if ($_GET["edit"] == "failed"):
    ?>
                <script>
                    M.toast({html: 'Could not save changes!', classes: 'aurora-red'})
                </script>
    <?php
            endif;
        endif;
    ?>
</main>
</body>
</html>
