<?php
    if (isset($amount)){
        $query = "SELECT post.*, user.username FROM post lEFT JOIN user ON user.user_id = post.user_id WHERE post.archived = 0 ORDER BY post.created_at DESC";
        $limit = $amount;
    }
    elseif (isset($username)){
        $query = "SELECT post.*, user.username FROM post LEFT JOIN user ON post.user_id = user.user_id WHERE post.user_id = (SELECT user_id FROM user WHERE username = ?) ORDER BY post.created_at DESC";
        $params = [$username];
    }
    elseif (isset($post_id)){
        $query = "SELECT post.*, user.username FROM post LEFT JOIN user ON post.user_id = user.user_id WHERE post.post_id = ? ORDER BY post.created_at  DESC";
        $params = [$post_id];
    }
    else {
        Header("Location: ../pages/");
        exit();
    }

    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
    $rows = $db->read($query, isset($params) ? $params : null, isset($limit) ? $limit : null);

    include("../helpers/timestamp.php");

    if (isset($rows)){
        foreach($rows as $row){
            if($row["username"] != ""){
                $username = $row["username"];
            }
            else {
                $username = "[unknown user]";
            }
            $timestamp = getDynamicTimestamp($row["created_at"]);
            if (isset($row["image_path"]) && $row["image_path"] != ""){
                include("../components/image_post.php");
            }
            else{
                if (isset($row["title"]) && $row["title"] != "") {
                    include("../components/text_post.php");
                }
                else if (isset($edit) && $edit){
                    include("../components/text_post.php");
                }
            }
        }
    }

