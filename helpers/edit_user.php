<?php
    session_start();
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
    if(!isset($_SESSION["loggedIn"]) && !$db->isAdmin($_SESSION["username"])){
        Header("Location: ../pages/");
        exit();
    }

    if(isset($_GET["id"]) && isset($_GET["action"])) {
        $id = $_GET["id"];
        if ($_GET["action"] == "update") {
            include("validation.php");
            $firstName = htmlentities($_POST["firstName"]);
            $lastName = htmlentities($_POST["lastName"]);
            $email = htmlentities($_POST["email"]);
            $query = "UPDATE user SET firstname=?, last_name=?, email=? WHERE user_id=?";
            $params = [htmlentities($_POST["firstName"]), htmlentities($_POST["lastName"]), htmlentities($_POST["email"]), $id];
            $success = checkFirstName() && checkLastName() && checkEmail();

            if ($success){
                $db->write($query, $params);
                $db->updateDisplayName();
                if (isset($_GET["redirect"]) && $_GET["redirect"] == "settings"){
                    header("Location: ../pages/settings.php?edit=success");
                    exit();
                }
                Header("Location: ../pages/users.php?action=saved");
                exit();
            }
            else{
                if (isset($_GET["redirect"]) && $_GET["redirect"] == "settings"){
                    header("Location: ../pages/settings.php?edit=failed");
                    exit();
                }
                Header("Location: ../helpers/edit_user.php?id=$id&action=edit&success=false");
                exit();
            }
        }
        else if ($_GET["action"] == "userEdit") {
            include("validation.php");
            $firstName = htmlentities($_POST["firstName"]);
            $lastName = htmlentities($_POST["lastName"]);
            $email = htmlentities($_POST["email"]);
            $query = "UPDATE user SET firstname=?, last_name=?, email=? WHERE user_id=?";
            $params = array(htmlentities($_POST["firstName"]), htmlentities($_POST["lastName"]), htmlentities($_POST["email"]), $id);
            $success = checkFirstName() && checkLastName() && checkEmail();

            if ($success){
                $db->write($query, $params);
                Header("Location: ../pages/users.php?action=saved");
                exit();
            }
            else{
                Header("Location: ../helpers/edit_user.php?id=$id&action=edit&success=false");
                exit();
            }
        }
        else if ($_GET["action"] == "edit"){
            include("../pages/user_form.php");
        }
        else if ($_GET["action"] == "block") {
            $db->write("UPDATE user SET active = 0 WHERE user_id = ?", [$id]);
            Header("Location: ../pages/users.php");
        }
        else if ($_GET["action"] == "unblock") {
            $db->write("UPDATE user SET active = 1 WHERE user_id = ?", [$id]);
            Header("Location: ../pages/users.php");
        }
        else if ($_GET["action"] == "promote") {
            $db->write("UPDATE user SET admin = 1 WHERE user_id = ?", [$id]);
            Header("Location: ../pages/users.php");
        }
        else if ($_GET["action"] == "demote") {
            $db->write("UPDATE user SET admin = 0 WHERE user_id = ?", [$id]);
            Header("Location: ../pages/users.php");
        }
        else if ($_GET["action"] == "delete") {

            $db->write("DELETE FROM user WHERE user_id = ?", [$id]);
            Header("Location: ../pages/users.php");
        }
        else if ($_GET["action"] == "setDisplayName") {
            if (isset($_POST["displayName"])){
                $displayName = htmlentities($_POST["displayName"]);
                $db->write("UPDATE user SET display_name=? WHERE user_id = ?", [$displayName, $id]);
                $db->updateDisplayName();
                header("Location: ../pages/settings.php?edit=success&tab=appearance");
                exit();
            }
        }
    }
