<?php
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
    if (!isset($_SESSION["loggedIn"]) && $db->isAdmin($_SESSION["username"])){
        Header("Location: ../pages/");
        exit();
    }
?>

<div class="row hide-on-med-and-down"></div>
<div class="row">
    <div class="col s0 m1 l2"></div>
    <div class="col s12 m10 l8">
        <ul class="collection">
            <?php
                $rows = $db->read("SELECT user.*, COUNT(post.post_id) AS post_count FROM user LEFT JOIN post ON post.user_id = user.user_id GROUP BY user.user_id");
                foreach($rows as $row){
                    include("../components/user_item.php");
                }
            ?>
        </ul>
    </div>
</div>
